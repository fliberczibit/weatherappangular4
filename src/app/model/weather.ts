export interface Weather {
  names: string;
  icon: string;
  temperature: number;
  temperatureF: number;
  summary: string;
}
