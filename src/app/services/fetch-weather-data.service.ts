import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/first';
import {Weather} from '../model/weather';

const GEOLOCATION_ERRORS = {
  'errors.location.unsupportedBrowser': 'Browser does not support location services',
  'errors.location.permissionDenied': 'You have rejected access to your location',
  'errors.location.positionUnavailable': 'Unable to determine your location',
  'errors.location.timeout': 'Service timeout has been reached'
};

@Injectable()

export class FetchWeatherDataService {
  constructor(private http: HttpClient) {
  }

  /*Get Promise with weather*/
  getWeatherData(): Promise<Weather> {
    return this.getPosition()
      .then((position) => this.http.get(this.getWeatherUrl(position)).toPromise())
      .then(response => this.mapResponseToWeather(response));
  }

  private getWeatherUrl(position: Position): any {
    return 'https://fcc-weather-api.glitch.me/api/current?lat='
      + position.coords.latitude + '&lon=' + position.coords.longitude;
  }

  /*Get geogeolocation as Promise*/
  private getPosition(): Promise<Position> {
    return new Promise((resolve, reject) => {
      window.navigator.geolocation.getCurrentPosition(
        (position: Position) => resolve(position),
        (error: PositionError) => reject(error)
      );
    });
  }

  private mapResponseToWeather(response: any): Weather {
    return {
      names: response.name,
      icon: response.weather[0].icon,
      temperature: response.main.temp,
      temperatureF: (((response.main.temp) * 9) / 5) + 32,
      summary: response.weather[0].main
    };
  }
}
