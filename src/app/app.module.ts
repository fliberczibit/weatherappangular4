import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {WeatherInfoComponent} from './weather-info/weather-info.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {FetchWeatherDataService} from './services/fetch-weather-data.service';
import {HttpClientModule} from '@angular/common/http';
import  {GetGeolocationService} from './services/get-geolocation.service';
import { ObservablesComponent } from './observables/observables.component'
@NgModule({
  declarations: [
    AppComponent,
    WeatherInfoComponent,
    ObservablesComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    HttpClientModule
  ],
  providers: [
    FetchWeatherDataService,
    GetGeolocationService

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
