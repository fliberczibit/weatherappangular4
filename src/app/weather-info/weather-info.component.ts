import {Component, OnInit} from '@angular/core';
import {FetchWeatherDataService} from '../services/fetch-weather-data.service';
@Component({
  selector: 'app-weather-info',
  templateUrl: './weather-info.component.html',
  styleUrls: ['./weather-info.component.css']
})
export class WeatherInfoComponent implements OnInit {
  public weatherData: any;

  constructor(private fwds: FetchWeatherDataService) {
  }

  ngOnInit() {
    this.fwds.getWeatherData()
      .then((weatherData) => this.weatherData = weatherData);
  }

}
